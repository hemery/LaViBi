<?php
/**
 * \file 404.php
 * \brief Page 404 du site web
 * \date 15 mars 2017
 * \author Simon Hemery
 */
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<link rel="icon" type="image/png" href="img/logo.svg" />
		<link rel="stylesheet" href="structure.css">
                <link rel="stylesheet" href="animation.css">
		<link rel="stylesheet" href="404.css">
		<title>La Vilaine Bidouille - Page non trouvée</title>
<!-- ViewPort pour les mobiles -->
		<meta name="viewport" content="width=device-width" >
		
	</head>
	
	<body>
	  <header>
	      <!-- Titre de la page -->
	 <?php
	$title = "404"; 
	include("barre_logo.php");
	?>
	  </header>

        <!-- Le smiley :( -->
	  <div id="smiley">:(</div>
	  <!-- Le texte d'erreur -->
	  <p>Erreur 404. Vous avez essayer d'accéder à une page inconnue.</p>
	
	 
	    
        <!-- Le pied de page -->
	  	  <footer>
  <?php 
	$color = "white";
	$level = 0;
	include("retour.php");
  ?>
  </footer>
	</body>
</html>
