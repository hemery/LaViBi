<?php
/**
 * \file carrefour.php
 * \brief Page sur le carrefour des idées
 * \date 16 mars 2017
 * \author Simon Hemery
 */
?>
<!-- Page information du site -- CC BY-NC-SA -- FabLab La Vilaine Bidouille -->
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<link rel="icon" type="image/png" href="img/logo.svg" />
		<link rel="stylesheet" href="structure.css">
		<link rel="stylesheet" href="projet.css">
                <link rel="stylesheet" href="animation.css">
		<title>La Vilaine Bidouille - Carrefour des idées</title>
<!-- ViewPort pour les mobiles -->
		<meta name="viewport" content="width=device-width" ></code>
	</head>
	
	<body>
	  <header>
	    <?php 
		$title = "Le projet";
		$level = 2;
		include("barre_logo.php");
 		?>
	    <h3>Un carrefour des idées</h3>
	  </header>
	  <section>
	    <class id="idee"><img src="img/carrefour_idée.svg"></class>
	  <p>Il est difficile de prendre la mesure de toute la richesse humaine des membres d'un <a href="https://fr.wikipedia.org/wiki/Fab_lab">Fablab</a> qui s'avère être un véritable carrefour des intelligences. La diversité des parcours humains, professionnels mais également les compétences glanées grâce aux passions de chacun... Notre Fablab sort à peine du carton et déjà nous comptons dans nos rangs : cartographe, menuisier, maçon, électronicien, maquettiste naval, coutelier, automaticien, architecte, docteur en sciences humaines, géomaticien, programmeur, couturière, etc.
Toute cette intelligence rassemblée autour de la même volonté de <em>fabriquer</em> ensemble.</p>
<p>Cette diversité, présente nulle part ailleurs, est une richesse à l'intérieur du <a href="https://fr.wikipedia.org/wiki/Fab_lab">Fablab</a> mais elle fait également du <a href="https://fr.wikipedia.org/wiki/Fab_lab">Fablab</a> un interlocuteur suffisamment varié pour pouvoir discuter avec l'ensemble des intelligences présentes sur le territoire. Hightech, Lowtech, entreprises, politiques, monde de l'éducation, associatifs, chercheurs, jeune public. Il y aura toujours quelqu'un dans les rangs du Fablab pour avoir des idées éclairées.</p>
<p>C'est précisément pour cette raison que le Fablab doit constituer un Carrefour des idées et permettre de développer l'intelligence collective au sein du territoire en créant autour du "faire" des synergies entre les différents acteurs.</p>
<h4>Répondre aux différentes sollicitations</h4>
<p>des particuliers, des artisans, de designers, des entreprises, des collectivités en ménageant des temps propres à la satisfaction de chacun. Pourquoi pas, par exemple, un Pro Fablab dont les modalités d'accès aux machines, ou à certaines formations, pourraient être repensées au plus près du besoin des professionnels ?</p>
<h4>Organiser des rencontres croisées</h4>
<p>en vue de multiplier la vitesse des idées, d'augmenter le nombre de leurs collisions et de faire du <a href="https://fr.wikipedia.org/wiki/Fab_lab">Fablab</a> un vrai bouillon de création pourquoi ne pas penser à un Café Lab? Certaines conférences, pointues ou non, pourraient y avoir lieu. Les soirées <strong>présentations des projets</strong> pourraient également trouver leurs places dans ce cadre.</p>
<h4>Permettre à des gens de faire appel à cette diversité des intelligences présente au sein du <a href="https://fr.wikipedia.org/wiki/Fab_lab">Fablab</a></h4>
<p>Nul doute qu'il pourrait être intéressant pour différentes entités d'être en capacité de "saisir" le Fablab d'un certain nombre de questions via l'organisation d'audits dans les entreprises par exemple ou via l'organisation de rencontres coopératives sur des sujets précis.</p>
	  </section>
	  <footer>
  <?php 
include("retour.php");
 ?>
  </footer>
	</body>
</html>
