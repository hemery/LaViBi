# Présentation du projet
![Logo_LAVB](img/logo.png)
[http://www.fablabredon.org](http://www.fablabredon.org)

Dépot git du site web du FabLab du Pays de Redon "La Vilaine Bidouille". Le site web est actuellement en version 1.3.

## Qu'est ce qu'un FabLab ?
Le principe des fab labs est le partage libre d'espaces, de machines, de compétences et de savoirs, généralement dans un lieu unique.

## Qu'est ce que la Vilaine Bidouille ?
La Vilaine Bidouille est un FabLab de type associatif créée à Allaire dans le Pays De Redon.
Le siège social de l'association est située dans la commune de Bains-sur-Oust.
# Comment contribuer ?
Vous pouvez lire le [guide](CONTRIBUTING.md).
# Compatibilités des navigateurs

_Dernière mise à jour le 15 mars 2017_

| Navigateur | Version pour ordinateur | Version pour mobile |
|------------|-------------------------|---------------------|
| Firefox    | OK pour version 52      |    OK               |
| Chrome     | OK pour version 57      |    (non testé)      |
| Opera      | OK pour version 36      |    (non testé)      |
| Konqueror  | (non testé)             |                     |
| Links      | (non testé)             |                     |
| Safari     | (non testé)             |    (non testé)      |
| Internet Explorer | Non pour version inférieur à 9 |                   |
| Edge       |  (non testé)  | (non testé) |

# Technologies utilisés
Le site est basée sur actuellement 4 langages de programmations :
* HTML (Version 5) pour la structure du site.
![HTML5](img/HTML5_Logo_128.png)
* CSS (Version 3) pour l'apparence du site.
![CSS3](img/css3_logo.png)
* PHP pour la génération des pages web (calculé par le serveur web).
![PHP](img/logo_php.png)
* JavaScript pour les animation du site (calculé par le navigateur web).
![JavaScript](img/js-logo.png)
* SVG pour les images du site au format [vectorielle](https://fr.wikipedia.org/wiki/Image_vectorielle).
![SVG](img/svg-logo-v.png)

Le guide de contribution et le readme utilise le language [Markdown](https://fr.wikipedia.org/wiki/Markdown). La documentation est générer par le logiciel [Doxygen](http://www.stack.nl/~dimitri/doxygen/).

Anciènement il y avais un forum qui utilisait le CMS [PHPBB](http://www.phpbb-fr.com/) et était héberger chez [OVH](https://www.ovh.com/fr/).
Le site web est lui aussi héberger chez [OVH](https://www.ovh.com/fr/).

Le blog utilise le CMS [WordPress](https://fr.wordpress.org/) et est hébergé sur le site [Wordpress.com](https://fr.wordpress.com/).

# C'est quoi un CMS ? Pourquoi ne pas avoir utilisée qu'un seul CMS pour tout le site ?
Les CMS (Content Management System) ou Système de gestion de contenu permettent de facilité énormément la création d'un site web en clair ca permet d'éviter d'écrire du code et de construire le site via une interface graphique.
Il existe plein de CMS : Joomla !, Wordpress, PHPBB, Drupal, MediaWiki. 
Le choix de ne pas utiliser de CMS pour le site principal vien du fait que coder en dur permet un controle plus précise des fonctionnalité qu'on veut intégrer mais c'est aussi beaucoup plus compliquer qu'un CMS.

# Elle est ou la documentation ?
La documentation complète du site web est disponible dans le répertoire documentation au format html.