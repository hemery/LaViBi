<?php
/**
 * \file retour.php
 * \brief Affiche la flèche de retour en fonction des pages
 * \date 13 mars 2017
 * \author Simon Hemery
 */
?>
<!-- Cette page gère le bouton retour présent en fin des sous pages du site. -->
<?php
    $src="img/retour.svg";
$link = $_SERVER['HTTP_REFERER'];
if(preg_match("#projet.php|info.php#",$_SERVER['PHP_SELF']))
{
    $link = "index.php";
}
?>
<a href=<?php echo $link; ?> ><img src=<?php echo $src; ?>></a>
