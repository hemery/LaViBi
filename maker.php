<?php
/**
 * \file maker.php
 * \brief Page sur le fablab des Makers
 * \date 16 mars 2017
 * \author Simon Hemery
 */
?>
<!-- Page information du site -- CC BY-NC-SA -- FabLab La Vilaine Bidouille -->
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<link rel="icon" type="image/png" href="img/logo.svg" />
		<link rel="stylesheet" href="structure.css">
		<link rel="stylesheet" href="projet.css">
                <link rel="stylesheet" href="animation.css">
		<title>La Vilaine Bidouille - FabLab du Pays</title>
<!-- ViewPort pour les mobiles -->
		<meta name="viewport" content="width=device-width" ></code>
	</head>
	
	<body>
	  <header>
	    <?php
		$title = "Le projet"; 
		$level = 2;
		include("barre_logo.php"); 
		?> 
	    <h3>Un FabLab du Pays de Redon</h3>
	  </header>
	  <section>
	  <img src="img/fablabpays.png">
	    <p>L'appellation « Fablab de pays », telle que nous l'entendons, recouvre 4 dimensions :</p>
	    <h4>La reconnaissance des Makers du pays de Redon</h4>
	    <p>Reconnaissance dans tous les sens du terme : à la fois grâce à la localisation géographique de ces <a href="https://fr.wikipedia.org/wiki/Culture_maker">Makers</a>,  mais également de leurs disponibilités, de leurs talents et compétences, de leurs envies de partager leurs machines ou de transmetttre leurs savoirs.</p>
	    <p>Le fablab se propose de créer une « carte des Makers » du Pays de Redon, qu'ils soient <a href="https://fr.wikipedia.org/wiki/Low-tech">lowtech</a> ou <a href="https://fr.wikipedia.org/wiki/Techniques_de_pointe">hightech</a>, afin de permettre une synergie autour du « faire ». Cette carte pourra prendre plusieurs formes: site web, application pour smartphone. Elle pourra également donner lieu à des rencontres concrètes dans les ateliers entre makers ou avec des publics voulant apprendre à « faire »  ou à mieux « faire ».</p>
	    <h4>La mise en valeur du patrimoine redonnais</h4>
	    <p>Le pays de Redon possède un très riche patrimoine historique mais également humain et naturel.  Les nouvelles technologies peuvent permettre une mise en valeur de ce patrimoine. Cette volonté de transmission, qu'elle soit à destination des gens de Redon, des jeunes générations ou des touristes, donnera lieu à de magnifiques projets riches technologiquement et humainement. Entre la batellerie, la fonderie, la vannerie, les marais, le canal de Nantes à Brest; l'intérêt que nous portons aux cultures biologiques, à la permaculture,etc. Les projets seront légions.</p>
	    <p>Des projets commencent déjà  à émerger :
	      - La réalisation d'un <a href="https://fr.wikipedia.org/wiki/Jeu_s%C3%A9rieux">serious game</a> avec des élèves de la cité scolaire de Beaumont, des adultes du Proflab, du Fablab, et de la Fédé. -...</p>
	    <h4>Le rayonnement des énergies et des initiatives positives présentes sur le territoire</h4>
	    <p>Via la promotion de certains projets et la mise en relation des acteurs les plus dynamiques de notre territoire (individus, associations, établissements scolaires, entreprises, politiques) nous espérons bien participer à l'émergence de start-ups « made in Redon ». Nous connaissons tous des gens très créatifs pour qui un Fablab pourrait constituer un vrai coup de pouce.</p>
	    <p>Le Fablab fera tout pour mettre en avant et  faciliter l'émergence d'un certain de nombre de projets ambitieux ayant pour but de rayonner au-delà du pays de Redon.</p>
	    <h4>L'ouverture sur le reste du monde</h4>
	    <p>Le Fablab est un endroit ouvert, un lieu de partage, d'égalité et la notion de pays qu'il veut défendre doit être à son image. Un « pays » fait d'échanges et de rencontres avec tous. Nous espérons que Le Fablab pourra être moteur dans ces échanges non seulement avec le grand Ouest mais bien au-delà. Par exemple avec le réseau national et international des autres Fablabs.</p>
	    <em>
	      <p>Vous avez des projets qui sont en accord avec ces enjeux ?</p>
		  </em>
		  <div class="contact">
	      Contactez-nous !
		  </div>
	  </section>
	  <footer>
  <?php 
include("retour.php");
 ?>
  </footer>

	</body>
</html>
