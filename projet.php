<?php
/**
 * \file projet.php
 * \brief Page d'information sur le projet du FabLab
 * \date 16 mars 2017
 * \author Simon Hemery
 */
?>
<!-- Page information du site -- CC BY-NC-SA -- FabLab La Vilaine Bidouille -->
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<link rel="icon" type="image/png" href="img/logo.svg" />
		<link rel="stylesheet" href="structure.css">
		<link rel="stylesheet" href="projet.css">
		<link rel="stylesheet" href="animation.css">
<!-- ViewPort pour les mobiles -->
		<meta name="viewport" content="width=device-width" >
			<title>La Vilaine Bidouille - Projet</title>
	</head>
	
	<body>
	  <header>
	 <?php 
		$title="Le projet";
		$level = 1;
		include("barre_logo.php"); 
	?>
	  </header>
	  <section>
	    <p>Nous avons souhaité créer un <a href="https://fr.wikipedia.org/wiki/Fab_lab">Fablab</a> dans le pays de Redon parce que nous sommes absolument convaincus de l'importance du « faire » dans le développement d'un territoire.</p>
	    <p>
	      Nous pensons que la « bidouille » peut être un moyen ambitieux et efficace de tisser du lien social, de répandre et partager des connaissances, de lutter contre les inégalités, de promouvoir des initiatives positives, de permettre aux individus de gagner en autonomie, en capacité critique et en confiance en eux ...</p>
	    <p>C'est dans cet état d'esprit que nous voulons faire de ce <a href="https://fr.wikipedia.org/wiki/Fab_lab">Fablab</a> :</p>
		<em>N'hésitez pas à cliquer sur chacun de ces 4 axes pour avoir plus de précisions !</em>
	    <div id="partie">
		<figure>
		<a href="maker.php" title="Un FabLab du Pays de Redon"><img src="img/fablabpays.png"></a>
		<figcaption>Un FabLab du Pays de Redon</figcaption>
		</figure>
		<figure>
	    <a href="fabrique.php" title="Une fabrique de projets"><img src="img/engrenage.svg"></a>
		<figcaption>Une fabrique de projets</figcaption>
		</figure>
		<figure>
		<a href="educatif.php" title="Un FabLab éducatif" ><img src="img/éducation.svg"></a>
		<figcaption>Un FabLab éducatif</figcaption>
		</figure>
		<figure>
	    <a href="carrefour.php" title="Un carrefour des idées"><span id="carrefour"><img src="img/carrefour_idée.svg"></span></a>
		<figcaption>Un carrefour des idées</figcaption>
		</figure>
		</div>
	  </section>
	  	  <footer>
  <?php 
	include("retour.php");
 ?>
  </footer>
	</body>
</html>
