<?php
/**
 * \file detail_formation.php
 * \brief Page sur le detail de la formation
 * \date 16 mars 2017
 * \author Simon Hemery
 */
?>
<html>
	<head>
		<meta charset="utf-8"/>
		<link rel="icon" type="image/png" href="img/logo.svg" />
		<link rel="stylesheet" href="structure.css">
		<link rel="stylesheet" href="info.css">
                <link rel="stylesheet" href="animation.css">
     <link rel="stylesheet" href="detail_formation.css">
		<title>La Vilaine Bidouille - Infos</title>
<!-- ViewPort pour les mobiles -->
		<meta name="viewport" content="width=device-width" >
		
	</head>
     <body>
     <header>
     <?php
     $title = "Les infos";
$level= 2;
include("barre_logo.php");
?>
</header>
<h3>Programme détailliée</h3>
<h4>Introduction mon pc mon usine</h4>
<div id="date">4 mars 2017</div>
<div id="intervenant">Par Cyrille Laulier</div>
<img id="pc" src="img/pc.jpg"/>
<p>Où il sera expliqué au gens qu'un simple PC connecté à
internet est en réalité une usine de production.
Après un bref rappel historique sur le mouvement des
Makers, du cours du Neil Gershenfeld du MIT jusqu'aux
Makers Faires organisées partout dans le monde, nous
tenterons de prouver qu'un ordinateur connecté à in-
ternet offre à n'importe qui les ressources autrefois
réservées aux industriels.
</p>
<h4>Découverte de l'impression 3D</h4>
<div id="date">18 mars 2017</div>
<div id="intervenant">Par Claude Guihart, Yann Rollet et
François Gay de la Vilaine Bidouille</div>
<img id="impression3d" src="img/impression3D.png" />
<p>Avec l’impression 3D, Le futur est à portée de clic. Avez-
vous déjà rêvé de produire vos propres objets à la mai-
son ? D’appuyer sur un bouton pour voir apparaître
quelques minutes plus tard la pièce qu’il vous manque ?
de réparer à l’identique une pièce cassée sans avoir à
débourser des fortunes ? Cet atelier est pour vous ! Il
sera l'occasion de découvrir différentes machines et
techniques associées.
</p>
<h4>Présentation de GNU/Linux et des logiciels libres</h4>
    <div id="date">01 avril 2017</div>
    <div id="intervenant">Par les Mulots</div>
    <img id="linux" src="img/tux.png"/>
<p>Pour fêter le logiciel libre, Les Mulots proposent un
atelier de création de de clé USB Linux.
Repartez avec un système complet, sécurisé,
performant et LIBRE ! sur une clé USB bootable.
Démonstration de la création d’une clé Linux avec Fra-
makey.
Des clés seront proposées au prix de la clé USB 3.0 de
8Go soit 5€.
</p>
<h4>Retro Gaming avec Recalbox</h4>
<div id="date">8 avril 2017</div>
<div id="intervenant">Par Yann Rollet</div>
<img id="recalbox" src="img/recalbox.jpg"/>
<p>Retrouvez les consoles et les jeux des années 80-90
dans une mini console à fabriquer soi même grâce à
Recalbox et Raspberry pi. La Recalbox vous permet de
rejouer à tout un ensemble de consoles et de plateformes, dans votre salon, en toute simplicité ! Le système
recalboxOS est entièrement libre et
gratuit, et vous permet de créer
votre recalbox très simplement, grâce au Nano ordinateur, Raspberry pi.
Alors prêt a retrouver les petits
plombiers moustachus ?
</p>
<h4>Découverte d'Arduino</h4>
<div id="date">29 avril 2017</div>
<div id="intervenant">Par Cyrille Laulier</div>
<img id="arduino" src="img/arduino_uno.png"/>
<p>Les objets connectés, ça vous branche ? Vous rêvez de
construire un robot autonome, de contrôler votre mai-
son depuis votre mobile, ou encore de réaliser votre
propre guirlande de Noël ? Cette
initiation est pour vous ! elle va vous
permettre de découvrir la program-
mation d’une carte Arduino pour
connecter des composants et réali-
ser toutes sortes de projets allant de
la micro-robotique à l’art numéri-
que, en passant par la domotique,
l’électricité ou encore la mécanique.
</p>
<h4>Exploration de la Raspberry Pi</h4>
<div id="date">13 mai 2017</div>
<div id="intervenant">Par Thierry Thomas</div>
<img id="raspberry" src="img/raspberrypie.svg"/>
<p>Né en 2011, à l'époque proposé à 25$, cet ordinateur de
la taille d'une carte de crédit a révolutionné le monde de
l'informatique en quelques années. Créé pour initier les
jeunes à la programmation il a pulvérisé ses objectifs
initiaux: des milliers de projets dans des domaines di-
vers et variés ont vu le jour, des centaines d'éléments
matériels additionnels proposés à la vente et surtout
plus de 10 millions d'exemplaires
vendus à travers le monde en-
tier. Nous détaillerons les aspects
matériels et logiciel pour vous
convaincre du potentiel extraor-
dinaire de ce petit ordinateur
limité pour l'essentiel à votre
imagination.
</p>
<h4>Vibro Bot</h4>
<div id="date">03 juin 2017</div>
<div id="intervenant">Par Yohann Guard de l'association "la ferme
des écotais"</div>
<img id="vibrobot" src="img/vibrobot.jpg"/>
<p>Préparez-vous à entrer dans le
Futur et découvrez les prémices de la robotique en réalisant
un robot-brosse ou robot spirographe : juste à
partir d'une vieille brosse à
chaussure ou d'une boîte en
métal muni de crayons feutres
couplé à un moteur déaxé, vous fabriquerez votre premier robot dépoussiéreur ou dessinateur autonome.
Matériel nécessaire : Des brosses
(à dents, à chaussure ...), des petits
moteurs de jouets cassés, des supports de piles, des vibreurs de
vieux téléphone, des piles, des
pistolets à colle, des feutres et boites en métal + outils (tournevis et
pinces à dénuder)
Préparation préalable : Collecte et démontage.
</p>
<h4>La médiathèque virtuelle</h4>
<div id="date">17 juin 2017</div>
<div id="intervenant">Par Benjamin Thénon</div>
<img id="minecraft" src="img/minecraft.ico"/>
<p>Minecraft/Minetest,
vous connaissez ? Et
bien figurez-vous qu’il
peut être aussi un
formidable outil de
modélisation. Pour
repenser un bâtiment,
l’espace urbain voir
même concevoir un
objet. A l’image des
projets RennesCraft,
Morbicraft, Printcraft... que diriez-vous de commencer
ensemble à fonder un collectif de Crafteurs locaux et de
crafter la Médiathèque Jean Michel Bollé ?</p>
<h4>L'été des maker</h4>
<div id="date">01 juillet 2017</div>
<div id="intervenant">Par le collectif</div>
<img id="maker" src="img/maker.png"/>
<p>Où on apportera ses propres bidouilles en cours et où
on détaillera les activités estivales pour les makers.
Matériel nécessaire : du jus de pomme et des gâteaux
Préparation préalable : récupérer les agendas de Fablab
et des Makerspace de Bretagne.</p>
</section>
<footer>
<?php
include("retour.php");
?>
</footer>
</body>
</html>
