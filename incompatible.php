<?php
/**
 * \file incompatible.php
 * \brief Page d'avertissement d'incompatibilité du navigateur web
 * \author Simon Hemery
 * \date 13 mars 2017
 */
?>
<html>
	<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="img/logo.svg" />
	<link rel="stylesheet" href="incompatible.css">
	<title>La Vilaine Bidouille - Navigateur incompatible</title>
	</head>
	<body>
	<p>Il semblerai que votre navigateur soit incompatible avec le site de la Vilaine Bidouille.</p>
<p>Nous vous conseillons de télécharger
<a href="https://www.google.fr/chrome/browser/desktop/"> Chrome</a> ou <a href="https://www.mozilla.org/fr/firefox/new/">Firefox</a> qui sont compatible avec le site en substitue de votre ancien navigateur.</p>
<p>Ce sont tout les deux des logiciels gratuit !</p>
	<p>Et en plus libre pour Firefox !</p>
	<p>Choisisser votre camps cliquer sur une des deux icones présente ci-dessous !</p>
	<div id="chrome">
	<h1>Team Google Chrome</h1>
	<a href="https://www.google.fr/chrome/browser/desktop/"><img src="img/logo_chrome.svg"/></a>
	</div>
	<div id="firefox">
	<h1>Team Mozilla Firefox</h1>
	<a href="https://www.mozilla.org/fr/firefox/new/"><img src="img/logo_firefox.svg"/></a>
	</div>
	</body>
</html>
