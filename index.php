<?php
   // Condition qui détecte la version d'Internet Explorer 11 et effectue une redirection vers la page incompatible.php
   if (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') !== false) {
     header('Location: incompatible.php');
     exit();
   }
?>
<!--
  _            __      ___ _       _              ____  _     _             _ _ _      
 | |           \ \    / (_) |     (_)            |  _ \(_)   | |           (_) | |     
 | |     __ _   \ \  / / _| | __ _ _ _ __   ___  | |_) |_  __| | ___  _   _ _| | | ___ 
 | |    / _` |   \ \/ / | | |/ _` | | '_ \ / _ \ |  _ <| |/ _` |/ _ \| | | | | | |/ _ \
 | |___| (_| |    \  /  | | | (_| | | | | |  __/ | |_) | | (_| | (_) | |_| | | | |  __/
 |______\__,_|     \/   |_|_|\__,_|_|_| |_|\___| |____/|_|\__,_|\___/ \__,_|_|_|_|\___|
                                                                                      
  ___     _      _         _           
 | __|_ _| |__  | |   __ _| |__  
 | _/ _` | '_ \ | |__/ _` | '_ \  
 |_|\__,_|_.__/ |____\__,_|_.__/ 
                                  
								  
________                           _________          ________     _________             
___  __ \_____ _____  _________    ______  /____      ___  __ \__________  /____________ 
__  /_/ /  __ `/_  / / /_  ___/    _  __  /_  _ \     __  /_/ /  _ \  __  /_  __ \_  __ \
_  ____// /_/ /_  /_/ /_(__  )     / /_/ / /  __/     _  _, _//  __/ /_/ / / /_/ /  / / /
/_/     \__,_/ _\__, / /____/      \__,_/  \___/      /_/ |_| \___/\__,_/  \____//_/ /_/ 
               /____/         
			   
				                  _____
                                 |     |
                                 | | | |
                                 |_____|
                           ____ ___|_|___ ____
                          ()___)         ()___)
                          // /|           |\ \\
                         // / |           | \ \\
                        (___) |___________| (___)
                        (___)   (_______)   (___)
                        (___)     (___)     (___)
                        (___)      |_|      (___)
                        (___)  ___/___\___   | |
                         | |  |           |  | |
                         | |  |___________| /___\
                        /___\  |||     ||| //   \\
                       //   \\ |||     ||| \\   //
                       \\   // |||     |||  \\ //
                        \\ // ()__)   (__()
                              ///       \\\
                             ///         \\\
                           _///___     ___\\\_
                          |_______|   |_______| 
						  
Avertissement : Cette page est été codée par un humain et non une machine. Désolée pour les fautes qu'il peut y avoir.
Pour des réclamation sur des fautes d'orthographes ou des problèmes de choix de designs les postez sur le dépot git du site dans la section issue. -->
<!-- Page principal du site -->
<!DOCTYPE html>
<html xmlns:og="http://ogp.me/ns#">
<!-- Cette partie de code gère uniquement les caractéristique de la page ce n'est pas afficher sur la page -->
	<head>
        
        <!-- Fixe l'encodage de la page en UTF-8 -->
		<meta charset="utf-8"/>

        <!-- Une courte description du site (très utile pour le référencement par Google) -->
		<meta name="description" content="Le site de la Vilaine Bidouille. Découvrez les projets du FabLab du Pays de Redon." />
		
		<!-- Metadescription Open Graph -->
		<meta property="og:title" content="Accueil de La Vilaine Bidouille" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="http://www.fablabredon.org/" />
		<meta property="og:image" content="img/logo.svg" />
		<meta property="og:locale" content="fr_FR" />
		<meta property="og:description" content="Le site de la Vilaine Bidouille. Découvrez les projets du FabLab du Pays de Redon." />
		<meta property="og:site_name" content="La Vilaine Bidouille" />

		<!-- Style de l'index  -->
		<link rel="icon" type="image/png" href="img/logo.svg" />
		<!-- Charge le style de la page -->
		<link rel="stylesheet" href="index.css">
		<link rel="stylesheet" href="animation.css">
		
		<!-- Charge les animation -->
	   <link rel="stylesheet" href="animation.css">
		
		<!-- Charge les animation -->
		<link rel="stylesheet" href="creativecommons.css">
		
		<!-- ViewPort pour les mobiles -->
		<meta name="viewport" content="width=device-width" >
		
		<!-- Titre de la page (affficher en haut de la page)-->
		<title>La Vilaine Bidouille - FabLab du Pays de Redon</title>
		
	</head>
	<!-- Corps de la page -->
	<body>

		<!-- L'en tête ou est afficher le titre -->
		<!-- Le logo du site est générée automatiquement par la page barre_logo.php en fonction de différent critère -->
		<header>
		<?php
			include("barre_logo.php"); 
		?>
		</header>
		
		<!-- La barre de navigation -->
		<nav>
		    
		    <!-- Les liens -->
		    <span class="projet"/>
		        <a href="projet.php">Le Projet</a>
		    </span>
		    <!-- Ce code javascript permet d'ouvrir le lien "Les réalisations" dans une nouvelle fenêtre -->
		    <a href="https://lavilainebidouille.wordpress.com" onclick="window.open(this.href); return false;">Les réalisations</a>
		    <a href="info.php">Les infos pratiques</a>
		    
		</nav>
		
		<!-- Pied de page -->
		<footer>
		    
		    <!-- License Creative Commons générer automatiquement par le site de la creative commons -->
		    <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
		    <img alt="Licence Creative Commons" style="border-width:0" src="img/ccbync.svg" /></a>
		    <br />
		    <span class="name">
		        <span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/InteractiveResource" property="dct:title" rel="dct:type">
		            <strong>
		            Le site de La Vilaine Bidouille
		            </strong>
		        </span>
		    </span>
		    de 
		    <span class="website">
		        <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">
		            <strong>
		            www.fablabredon.org
		            </strong>
		        </span>
		    </span>
		    est mis à disposition selon les termes de la 
		    <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
		      licence Creative Commons Attribution - Pas d'Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>. Le code source du site web est disponible sur <a href="https://framagit.org/hemery/LaViBi">framagit</a>.
		    <br>Vous pouvez proposez des nouvelles fonctionnalitées ou rapporter un bug via ce <a href="https://framagit.org/hemery/LaViBi/issues">lien</a>, vous devez être inscrit sur framagit auparavant.
            <!-- Le numéro de version du site -->
		    <strong>Version 1.3</strong>
		
		</footer>
		
	</body>
</html>
