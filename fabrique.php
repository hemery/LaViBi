<?php
/**
 * \file fabrique.php
 * \brief Page sur le fablab la fabrique de projet
 * \date 16 mars 2017
 * \author Simon Hemery
 */
?>
<!-- Page information du site -- CC BY-NC-SA -- FabLab La Vilaine Bidouille -->
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<link rel="icon" type="image/png" href="img/logo.svg" />
		<link rel="stylesheet" href="structure.css">
		<link rel="stylesheet" href="projet.css">
                <link rel="stylesheet" href="animation.css">
		<title>La Vilaine Bidouille - Fabrique de Projets</title>
<!-- ViewPort pour les mobiles -->
		<meta name="viewport" content="width=device-width" ></code>
	</head>
	
	<body>
	  <header>
	    <?php
		$title = "Le projet"; 
		$level = 2;
		include("barre_logo.php"); 
		?>
	    <h3>Un fabrique de projets</h3>
	  </header>
	  <section>
	  <img src="img/engrenage.svg">
	  <p>Dans un Fablab on fabrique. C'est le coeur !</p>
	  <p>Le Fablab essaiera d'accompagner au mieux de ses capacités tous les désirs de <strong>bidouille</strong> (jusqu'aux projets industriels les plus ambitieux).
	  Tous les projets seront les bienvenus (dans la limite du respect de la charte* des Fablabs). Il n'y a pas de "petits" projets. Et même si vous n'avez pas encore d'idée précise de ce que vous voulez fabriquer, venez bricoler et vous ne resterez pas longtemps désoeuvrés...</p>
	  <p>La mission d'un Fablab est donc d'abord de proposer une réelle capacité de Fabrication, grâce à la mise à disposition d'un lieu (ou plusieurs lieux ayant chacun ces spécificités), d'un parc de machines et de gens capables de former les makers à leur utilisation.</p>
	  <p>Mais un Fablab c'est également un endroit où faire naître, où supporter, où promouvoir un certain nombre de projets.</p>
	  <p>Faire naître un projet c'est par exemple répondre à une demande d'une entreprise ou d'une collectivité qui saisirait le Fablab pour une commande particulière. Cela peut se faire à l'occasion de l'organisation de rencontres coopératives avec différents acteurs, etc.</p>
	  <h4>Supporter un projet cela passera par : </h4>
	  <ul>
		<li>Faire en sorte que chaque porteur de projet puisse acquérir la compétence pour réaliser lui-même ce qu'il a en tête. Le former à l'usage de telle ou telle machine, de certains programmes informatiques, outils, etc. (via des formations ou simplement en échangeant avec d'autres makers.)</li>
		<li>Faire en sorte que chaque bidouilleur puisse bénéficier de la compétence, de la critique, de l'expertise ou même de la naïveté des membres du Fablab (mais également des membres du réseau des Makers du pays de Redon et des autres Fablabs). Le Fablab pourrait organiser à cet effet des soirées de "présentation des projets du Fablab".</li>
	  </ul>
	  <h4>Promouvoir un projet cela signifierait également :</h4>
	  <p>Lui offrir une plus grande visibilité via une vitrine Web dédiée aux projets.</p>
	  <p>Lui permettre de rencontrer un public plus vaste lors de l'organisation ou de la participation à des événements type Makerfaire.</p>
	  <p>Nous aimerions pouvoir également organiser un concours du "projet le plus prometteur".</p>
	  <p>
Mais surtout cette Fabrique du Fablab sera à l'image de ce que les gens décideront d'y faire. </p>
<div class="contact">Soyez ambitieux et créatifs!</div>
	  
	  </section>
	  <footer>
 <?php 
	include("retour.php");
 ?>
  </footer>
	</body>
</html>
