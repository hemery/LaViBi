<?php
/**
 * \file educatif.php
 * \brief Page sur le fablab educatif
 * \date 16 mars 2017
 * \author Simon Hemery
 */
?>
<!-- Page information du site -- CC BY-NC-SA -- FabLab La Vilaine Bidouille -->
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<link rel="icon" type="image/png" href="img/logo.svg" />
		<link rel="stylesheet" href="structure.css">
		<link rel="stylesheet" href="projet.css">
                <link rel="stylesheet" href="animation.css">
		<title>La Vilaine Bidouille - FabLab éducatif</title>
<!-- ViewPort pour les mobiles -->
		<meta name="viewport" content="width=device-width" ></code>
	</head>
	
	<body>
	  <header>
	    <?php
		$title = "Le projet";
		$level = 2; 
		include("barre_logo.php"); 
		?>
	    <h3>Un FabLab éducatif</h3>
	  </header>
	  <section>
		<img src="img/éducation.svg"/>
		<h4>Une vraie mission d'éducation</h4>
		<p>La volonté de faire circuler un certain nombre de savoirs et de savoir-faire, numériques mais pas seulement, est clairement une des missions essentielles que nous avons voulu nous fixer. </p>
		<p>Réduire la fracture numérique, permettre un échange plus fluide entre différentes compétences, développer à la fois l'autonomie et l'esprit critique, casser les frontières trop rigides en France entre les diplômés/non diplômés ou  manuels/intellectuels, donner aux individus les moyens de leurs envies de créer et de bidouiller, etc.</p>
		<p>Voilà ce qui nous anime profondément.</p>
		<h4>Un besoin réel</h4>
		<p>De multiples publics sont demandeurs : </p>
		<ul>
<li>Le public des plus jeunes (apprentissage du codage, de la programmation, dès le plus jeune âge par exemple) ou des collégiens/lycéens.</li>
<li>Les publics en difficulté (qui doivent se former à de nombreuses évolutions de la société et pour qui la <a href="https://fr.wikipedia.org/wiki/Litt%C3%A9ratie">littératie</a> est un enjeu essentiel). Pour ces publics, le numérique peut représenter une vraie chance de réinsertion et de réussite sociale (cf l'action d'association comme simplon.co, par exemple).</li>
<li>Les personnes âgées pour qui l'apprentissage du numérique est l'occasion à la fois de rester connectées au monde qui les entoure et de solliciter leurs capacités cérébrales. Sans parler que les <em>anciens</em> possèdent un savoir Lowtech que nous gagnerions à valoriser auprès des Makers.</li>
<li>Les entreprises et leurs salariés. Un Fablab est l'occasion pour les salariés de développer leur propre créativité et pour les entreprises de continuer à avoir des salariés enthousiastes.</li>
<li>Les associations qui peuvent avoir besoin dans le cadre de leurs activités d'acquérir rapidement certaines compétences.</li>
<li>Les particuliers voulant se mettre à niveau dans tel ou tel domaine technique ou qui ont simplement besoin d'une compétence précise pour aller plus loin dans leur projet.</li>
<p>Dans un monde qui va très vite, la formation initiale ne peut plus suffire à assurer la capacité d'un individu à pouvoir intéragir toute la durée de sa vie avec ce qui l'entoure. La formation continue, et entre pairs, jouera un rôle de plus en plus déterminant. Le Fablab voudrait prendre sa part dans cette formation continue.</p>
<h4>Former à quoi ?</h4>
<p>
A tout ce dont on a besoin pour fabriquer <strong>presque tout</strong> ! 
A la programmation, à l'électronique, à l'usage et à la création de machines, à la science en général (via l'approfondissement des problèmes rencontrés dans les projets par exemple), au travail du bois, au travail graphique, à la réparation d'objets, etc, etc. 
Il n'y virtuellement aucune limite dans ce que l'on peut apprendre dans un Fablab. Les deux seules vraies limites viendront premièrement des gens qui "feront" au sein du Fablab, de leurs compétences et incompétences. 
Et deuxièmement du manque éventuel de moyens pour aller chercher à l'extérieur les compétences qui nous manquent.
</p>
<h4>Et comment ?</h4>
<p>Toutes les options sont envisageables : </p>
<ul>
<li>Au sein du Fablab par la création d'un poste d'animateur/formateur mais également lors de formations <strong>mobiles</strong> dans d'autres lieux comme les médiathèques ou les écoles par exemple.</li>
<li>Grâce à la création d'outils pédagogiques concrets ou en ligne via la mise en ligne de <a href="https://fr.wikipedia.org/wiki/Wiki">Wiki</a>, ou même de <a href="https://fr.wikipedia.org/wiki/Formation_en_ligne_ouverte_%C3%A0_tous">MOOC</a>.</li>
<li>Grâce à la création d'événements particuliers (des conférences, des week-ends d'échanges de compétences, l'organisation d'échange d'heures de cours entre makers, etc.), l'organisation de centres aérés autour de la bidouille, etc.</li>
<li>Tout dépendra de vos envies, de vos idées, et des moyens dont nous disposerons.</li>
</ul>
<h4>Avec qui ? Les partenaires potentiels</h4>
<p>Pour que notre Fablab puisse être à la hauteur de telles ambitions, il y faudra aussi l'énergie de nos partenaires. Et cela tombe bien car nous avons la chance d'avoir, dans le pays de Redon, un grand nombre de partenaires impliqués dans l'éducation.</p>
<h4>Quelques exemples ?</h4>
<ul>
<li>Le réseau des médiathèques</li>
<li>Les associations (la Fédé, clicnpuces, les hydrophiles, les mulots, un vélo pour l'afrique, les petits débrouillards présents sur Vannes,  la main à la pâte, etc.)</li>
<li>Les autres Fablabs très dynamiques du Grand Ouest.</li>
<li> Le monde éducatif : <a href="http://www.campus-redon-industries.com/">le Campus E.S.P.R.I.T. Industries</a>, les nombreux établissements scolaires du pays. Nous avons en outre  la chance à Redon d'avoir un <a href="http://www.proflab.org/">Proflab</a> (au sein de la cité scolaire de Beaumont).</li>
<li>Les artisans, et les <strong>makers</strong> professionnels qui sont légions.</li>
</ul>
<p>L'un des axes qui garantira le succès de ce Fablab éducatif tiendra dans sa capacité à fédérer toutes ces énergies et à faire appel à chacun des partenaires dans ce qu'il sait faire le mieux.
A nous de lancer des projets concrets et transversaux qui seront autant d'occasions de tisser des liens et de faire circuler le savoir.</p>
<p>Une fois encore : Le fablab sera à l'image des gens qui s'en saisiront alors si vous avez des idées, des projets, des envies.</p>
<div class="contact">
Contactez-nous !
</div>
	  </section>
	  <footer>
  <?php 
	include("retour.php");
 ?>
  </footer>
	</body>
</html>
