Comment contribuer ?
====================
# Je suis développeur je veux apporter mon aide

En local (via git)
------------------
![Git](img/git-logo.png)
Tout d'abord vous devez posséder le gestionnaire de version git.
### C'est quoi un gestionnaire de version ?
C'est un logiciel qui permet de stocker l'ensemble des fichiers d'un projet par exemple en conservant la chronologie de toute les modification qui ont été effectuées dessus.
### Comment installer git ?
#### Sous GNU/Linux
##### Debian et Ubuntu
```
apt-get install git
```
##### Fedora
```
yum install git
```
##### Gentoo
```
emerge --ask --verbose dev-vcs/git
```
##### Arch Linux
```
pacman -S git 
```
##### openSUSE
```
zypper install git
```
#### Sous BSD
##### FreeBSD
```
cd /usr/ports/devel/git
make install
```
##### OpenBSD
```
pkg_add git
```
#### Sous Microsoft Windows
Télécharger l'executable sur le site : https://git-for-windows.github.io/.

Installé le.

#### Sous Mac
Télécharger l'installateur graphique sur le site : http://sourceforge.net/projects/git-osx-installer/.

Installé le.

### Comment récupérer les sources du site web via git ?
```
git clone https://framagit.org/hemery/LaViBi.git
```
Via GitLab
----------
Tout dabord vous devez être inscrit sur GitLab.
Si vous êtes inscrit ou une fois inscrit vous pouvez contribuez en commencant par forker le projet.
Vous aurez ensuite une copie du dépot principal associée à votre compte.
Vous pouvez maintenant modifier directement les fichier du projet. 

### Si vous vous situer sur votre copie local du dépot
Une fois vos modification terminée vous pourez directement sauvegarder vos modification ce qui aurra pour effet d'enregistrer vos modification sur votre dépot local.
### Si vous vous situer sur le dépot principal
Une fois vos modification terminée quand vous enregistrer vos modification une nouvelle branche sera créée dans votre copie local et une nouvelle requete de fusion sera sur lancée sur le dépot principal.
Vous n'avez plus qu'a attendre que le propriétaire du dépot accepte vos modification.
Pour savoir si vous êtes bien sur le dépot principal lorsque vous enregistrer vos modification vous aurez un petit avertissement en plus que vous n'avez pas sur votre dépot local :
![CommitDepotPrincipal](img/gitlab_depotprincipal.png)
# Je veux simplement apporter des idées d'amélioration du site
Vous pouvez proposer des idées d'améliration du site via l'onglet Issues du dépot git. De clique sur l'icone new issue et de nommer votre issue.
Vous devez être inscrit sur framagit pour proposer une idée d'amélioration.