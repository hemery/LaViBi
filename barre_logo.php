<?php
/**
 * \file barre_logo.php
 * \brief Affiche le logo du site web en fonction des pages
 * \author Simon Hemery
 * \date 15 mars 2017
 * Affiche le logo du site web en fonction des pages
 */


 /**
 * \fn logo
 * \brief Affiche le logo du FabLab en fonction du titre de la page
 */
function display_logo()
{
    ?><img src="img/logo.svg" /><?php
}
function display_title()
{
    ?><h1>La Vilaine Bidouille</h1><?php
}
function display_subtitle()
{
    ?><h2>Le FabLab du Pays de Redon</h2><?php
}
function barre_logo_subpage($page_title)
{
    $link = $_SERVER['HTTP_REFERER'];
    if(preg_match("#projet.php#",$_SERVER['PHP_SELF']))
        {
            $link = "projet.php";
        }
    else if(preg_match("#info.php#",$_SERVER['PHP_SELF']))
        {
            $link = "info.php";
        }

    ?><nav><?php
   ?><a href="index.php"><?php display_logo();display_title();?></a>
    <a href="<?php echo $link; ?>"><h2><?php echo $page_title; ?></h2></a></nav><?php
}
function barre_logo_home()
{
    display_title();
    ?><div id="logo"><?php
    display_logo();
    ?></div><?php
    display_subtitle();
}

if(isset($title))
{
barre_logo_subpage($title);
}
else
    {
        barre_logo_home();
    }
		?>



