﻿<?php
/**
 * \file info.php
 * \brief Page d'information sur le FabLab
 * \date 16 mars 2017
 * \author Simon Hemery
 */
?>
<!-- Page information du site -- CC BY-NC-SA -- FabLab La Vilaine Bidouille -->
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<link rel="icon" type="image/png" href="img/logo.svg" />
		<link rel="stylesheet" href="structure.css">
		<link rel="stylesheet" href="info.css">
                <link rel="stylesheet" href="animation.css">
		<title>La Vilaine Bidouille - Infos</title>
<!-- ViewPort pour les mobiles -->
		<meta name="viewport" content="width=device-width" >
		
	</head>
	
	<body>
	  <header>
	   <?php 
		$title = "Les infos";
		$level = 1;
		include("barre_logo.php"); 
		?>
	  </header>
	  <section>
	<iframe frameBorder="0" src="https://framacarte.org/en/map/ou-trouver-la-fablab-la-vilaine-bidouille_4994?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=true&datalayersControl=true&onLoadPanel=undefined&captionBar=false">
	</iframe>
	<p>
	<a id="fullscreen" title="Voir en plein écran" href="https://framacarte.org/en/map/ou-trouver-la-fablab-la-vilaine-bidouille_4994" ><img src="img/fullscreen.svg">
	</a>
	</p>
	    <span id="horloge"><img src="img/info.svg"></span>
	  <p>Des <strong>formations</strong> sur différents sujets techniques sont proposées <strong>tous les 15 jours</strong> le <strong>samedi de 10h à 12h30</strong>  du <strong>4 mars au 1er juillet 2017</strong> à la <strong>médiathèque de Redon</strong> en <strong>salle d'animation</strong> à un <strong>tarif gratuit</strong> sur <strong>inscription</strong>.</p>
	  </p>
	
	  <strong>Venez nombreux pour vous former et partager vos bidouilles, vos créations, vos projets !</strong>
      <a href="detail_formation.php">Voir le programme détailié</a>
<p><a id="bouton" href="../doc/fablabprospectus.pdf"><img src="img/download.svg"><img src="img/book.svg">Télécharger le prospectus</a></p>
<p><a id="bouton" href="../doc/openbidouille_affiche.pdf "><img src="img/download.svg"><img src="img/book.svg">Télécharger l'affiche</a></p>
	  </section>
	  	  <footer>
  	<?php
	include("retour.php"); 
	?>
  </footer>
	</body>
</html>
